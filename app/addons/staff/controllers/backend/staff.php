<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Session;
use Tygh\Mailer;
use Tygh\Api;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    
    if ($mode == 'update' || $mode == 'add') {
        
        $staff_id = fn_update_staff($_REQUEST['staff_data'], $_REQUEST['staff_id']);
        if ($mode == 'add') {    
	    return array(CONTROLLER_STATUS_OK, "staff" . (($staff_id) ? '.manage' : '.add'));
        } else {
            return array(CONTROLLER_STATUS_OK, "staff" . (($staff_id) ? '.manage' : '.update?staff_id=' . $_REQUEST['staff_id']));
        }
    }

    if ($mode == 'delete') {
        
        fn_delete_staff($_REQUEST['staff_id']);
        return array(CONTROLLER_STATUS_REDIRECT, 'staff.manage');
    }
    
    if ($mode == 'm_delete') {

        if (!empty($_REQUEST['staff_ids'])) {
            foreach ($_REQUEST['staff_ids'] as $v) {
                fn_delete_staff($v);
            }
        }

        return array(CONTROLLER_STATUS_OK, 'staff.manage');
    }
    
}

if ($mode == 'manage') {
      
    list($staff, $search) = fn_get_staff($_REQUEST, Registry::get('settings.Appearance.admin_elements_per_page'));
    
    Tygh::$app['view']->assign('staff', $staff);
    Tygh::$app['view']->assign('search', $search);

} elseif ($mode == 'update' || $mode == 'add') {   
    
    if ($mode == "update") {    
        if (empty($_REQUEST['staff_id'])) {
	    return array(CONTROLLER_STATUS_NO_PAGE);
        } else {
            list($staff, $search) = fn_get_staff($_REQUEST, $auth, Registry::get('settings.Appearance.admin_elements_per_page'));
	    $member = $staff[0];
	    Tygh::$app['view']->assign('member', $member);
	    Tygh::$app['view']->assign('search', $search);
        }
    }     
    
} 