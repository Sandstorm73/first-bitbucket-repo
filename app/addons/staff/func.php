<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Navigation\LastView;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_get_staff_info() {
    list($staff, $search) = fn_get_staff();
    
    foreach ($staff as &$member) {
        if (!empty($member['user_id'])) {
            $user = fn_get_user_info($member['user_id']);
        
            if(empty($member['firstname'])) {
                $member['firstname'] = $user['firstname'];
            }
            if(empty($member['lastname'])) {
                $member['lastname'] = $user['lastname'];
            }
        }
    }
    
    return $staff;
}

function fn_get_staff($params = array(), $items_per_page = 0, $custom_view = '')
{
      
    $default_params = array (
        'page' => 1,
        'items_per_page' => $items_per_page
    );

    $params = array_merge($default_params, $params);

    // Define fields that should be retrieved
    $fields = array(
        "?:staff.staff_id",
        "?:staff.firstname",
        "?:staff.lastname",
        "?:staff.function",
        "?:staff.email",
        "?:staff.user_id"
    );

    // Define sort fields
    $sortings = array(
        'id' => "?:staff.staff_id",
        'function' => "?:staff.function",
        'email' => "?:staff.email",
        'name' => array("?:staff.firstname", "?:staff.lastname"),
        'user_id' => "?:staff.user_id",
    );

    
    $condition = array();
    $join = $group = '';

    if (!empty($params['staff_id'])) {
        $condition['staff_id'] = db_quote(" AND ?:staff.staff_id = ?i", $params['staff_id']);
    }
    if (!empty($params['user_id'])) {
        $condition['user_id'] = db_quote(" AND ?:staff.user_id = ?i", $params['user_id']);
    }
    if (!empty($params['firstname'])) {
        $condition['firstname'] = db_quote(" AND ?:staff.firstname = ?s", $params['firstname']);
    }
    if (!empty($params['lastname'])) {
        $condition['lastname'] = db_quote(" AND ?:staff.lastname = ?s", $params['lastname']);
    }
    if (!empty($params['email'])) {
        $condition['email'] = db_quote(" AND ?:staff.email = ?s", $params['email']);
    }
    if (!empty($params['function'])) {
        $condition['function'] = db_quote(" AND ?:staff.function = ?s", $params['function']);
    }
   
    $group .= " GROUP BY ?:staff.staff_id";
   

    $sorting = db_sort($params, $sortings, 'name', 'asc');

    
    // Paginate search results
    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(DISTINCT(?:staff.staff_id)) FROM ?:staff WHERE 1 ". implode(' ', $condition));
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $staff = db_get_array("SELECT " . implode(', ', $fields) . " FROM ?:staff WHERE 1" . implode(' ', $condition) . " $group $sorting $limit");

    foreach ($staff as &$member) {
        $member['main_pair'] = fn_get_image_pairs($member['staff_id'], 'staff', 'M', true, true);        
    }
        
        
    LastView::instance()->processResults('staff', $staff, $params);


    return array($staff, $params);
}

function fn_update_staff($params, $staff_id = '0') {
    
    if (!empty($params['user_id'])) {
	$user = fn_get_user_info($params['user_id']);
	if (empty($user)) {
	    fn_set_notification('E', __('error'), __('user_doesnt_exist'));
	    return false;
	}
	
	$staff = db_get_row("SELECT * FROM ?:staff WHERE user_id = ?i", $params['user_id']);
	if (!empty($staff)) {
	    if ($staff_id != 0) {
	        if ($staff_id != $staff['staff_id']) {
	            fn_set_notification('E', __('error'), __('already_used'));
	            return false;
	        }
	    } else {
		fn_set_notification('E', __('error'), __('already_used'));
	        return false;
	    }
	}
    
    }
    
    if ($staff_id != 0) {
	$staff = db_query("UPDATE ?:staff SET ?u WHERE staff_id = ?i", $params, $staff_id);
        fn_attach_image_pairs('staff_main', 'staff', $staff_id);
   	
    } else {
	$staff = db_query("INSERT INTO ?:staff ?e ", $params);
        fn_attach_image_pairs('staff_main', 'staff', $staff);
    }
    
    
    return $staff;
    
}
function fn_delete_staff($staff_id) {
    
    $staff = db_query("DELETE FROM ?:staff WHERE staff_id = ?i", $staff_id);
    
}
function fn_get_staff_email($staff_id = '0') {
    $params['staff_id'] = $staff_id;
    list($staff, $search) = fn_get_staff($params);
    
    $member = $staff[0];
    if (empty($member['email'])) {
        if(!empty($member['user_id'])) {
	    $user = fn_get_user_info($member['user_id']);
	    $email = $user['email'];
        } else {
	    $email = 'false';
        
        }
    } else {
        $email = $member['email'];
    }
    return $email;
}
