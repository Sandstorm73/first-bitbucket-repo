
{if $member}
    {assign var="id" value=$member.staff_id}
{else}
    {assign var="id" value=0}
{/if}



<form name="staff_form" action="{""|fn_url}" method="post" class="form-horizontal form-edit form-table" enctype="multipart/form-data">

{capture name="mainbox"}

    <input type="hidden" name="staff_id" value="{$id}" />

    {include file="common/subheader.tpl" title=__("staff_member_information")}

    <div class="control-group">
	<label for="firstname" class="control-label">{__("firstname")}:</label>
	<div class="controls">
	    <input type="text" id="firstname" name="staff_data[firstname]" class="input-large" size="32" maxlength="128" value="{$member.firstname}" />
	</div>
    </div>
    <div class="control-group">
	<label for="lastname" class="control-label">{__("lastname")}:</label>
	<div class="controls">
	    <input type="text" id="lastname" name="staff_data[lastname]" class="input-large" size="32" maxlength="128" value="{$member.lastname}" />
	</div>
    </div>
    <div class="control-group">
	<label for="email" class="control-label cm-email">{__("email")}:</label>
	<div class="controls">
	    <input type="text" id="email" name="staff_data[email]" class="input-large" size="32" maxlength="128" value="{$member.email}" />
	</div>
    </div>
    <div class="control-group">
	<label for="function" class="control-label">{__("staff_function")}:</label>
	<div class="controls">
	    <input type="text" id="function" name="staff_data[function]" class="input-large" size="32" maxlength="128" value="{$member.function}" />
	</div>
    </div>
    <div class="control-group">
	<label for="user_id" class="control-label">{__("user_id")}:</label>
	<div class="controls">
	    <input type="text" id="user_id" name="staff_data[user_id]" class="input cm-value-integer" size="32" maxlength="64" value="{$member.user_id}" />
	</div>
    </div>
    
    <div class="control-group">
        <label class="control-label">{__("image")}:</label>
        <div class="controls">
            {include file="common/attach_images.tpl" image_name="staff_main" image_object_type="staff" image_pair=$member.main_pair icon_text=__("text_staff_thumbnail") detailed_text=__("text_staff_detailed_image") no_thumbnail=true}
        </div>
    </div>
    


{/capture}

{if !$id}
    {assign var="_title" value="{__("new_staff_member")}"}
{else}
    {assign var="_title" value="{__("editing_staff_member")}"}
{/if}

{$_title = $_title|strip_tags}
{assign var="redirect_url" value="staff.manage"}

{capture name="buttons"}
    {capture name="tools_list"}
        <li>{btn type="list" text=__("delete") class="cm-confirm cm-post" href="staff.delete?staff_id=`$id`&redirect_url=`$redirect_url`"}</li>
    {/capture}
    {if $id && $smarty.capture.tools_list|trim !==""}
        {dropdown content=$smarty.capture.tools_list}
    {/if}
<div class="btn-group btn-hover dropleft">
    {if $id}
        {include file="buttons/save_changes.tpl" but_meta="dropdown-toggle" but_role="submit-link" but_name="dispatch[staff.`$runtime.mode`]" but_target_form="staff_form" save=$id}
    {else}
        {include file="buttons/button.tpl" but_text=__("create") but_meta="dropdown-toggle" but_role="submit-link" but_name="dispatch[staff.`$runtime.mode`]" but_target_form="staff_form" save=$id}
    {/if}
    
</div>

{/capture}

{include file="common/mainbox.tpl" title=$_title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}
</form>