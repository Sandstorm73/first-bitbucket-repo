
{capture name="mainbox"}

{assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}

<form action="{""|fn_url}" method="post" name="stafflist_form" id="stafflist_form" class="{if $runtime.company_id && !"ULTIMATE"|fn_allowed_for}cm-hide-inputs{/if}">
<input type="hidden" name="fake" value="1" />

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}

{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}

{if $staff}
<table width="100%" class="table table-middle">
<thead>
<tr>
    <th width="1%" class="center {$no_hide_input}">
        {include file="common/check_items.tpl"}</th>
    <th width="3%" class="nowrap"><a class="cm-ajax" href="{"`$c_url`&sort_by=id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("id")}{if $search.sort_by == "id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="18%"><a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("person_name")}{if $search.sort_by == "name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="30%"><a class="cm-ajax" href="{"`$c_url`&sort_by=email&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("email")}{if $search.sort_by == "email"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="26%"><a class="cm-ajax" href="{"`$c_url`&sort_by=function&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("staff_function")}{if $search.sort_by == "function"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th><a class="cm-ajax" href="{"`$c_url`&sort_by=user_id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("user_id")}{if $search.sort_by == "user_id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
   
</tr>
</thead>
{foreach from=$staff item=member}

    <td class="center {$no_hide_input}">
    <input type="checkbox" name="staff_ids[]" value="{$member.staff_id}" class="checkbox cm-item" /></td>
    <td><a class="row-status" href="{"staff.update?staff_id=`$member.staff_id`"|fn_url}">{$member.staff_id}</a></td>
    <td class="row-status">{if $member.firstname || $member.lastname}<a href="{"staff.update?staff_id=`$member.staff_id`"|fn_url}">{$member.lastname} {$member.firstname}</a>{else}-{/if}</td>
    {if $member.email}
	<td><a class="row-status" href="mailto:{$member.email|escape:url}">{$member.email}</a></td>
    {else}
 	<td>-</td>   
    {/if}
    <td class="row-status">{if $member.function}{$member.function}{else}-{/if}</td>
    <td class="row-status">{if $member.user_id}<a href="{"profiles.update?user_id=`$member.user_id`"|fn_url}">{$member.user_id}</a>{else}-{/if}</td>
    
    <td class="right nowrap">
        {capture name="tools_list"}
            
            <li>{btn type="list" text=__("edit") href="staff.update?staff_id=`$member.staff_id`"}</li>
            
            <li>{btn type="list" text=__("delete") class="cm-confirm cm-post" href="staff.delete?staff_id=`$member.staff_id`&redirect_url=`$return_current_url`"}</li>            
            
        {/capture}
        <div class="hidden-tools">
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
    
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl" div_id=$smarty.request.content_id}

{capture name="buttons"}
    {if $staff}
        {capture name="tools_list"}
            <li>{btn type="delete_selected" dispatch="dispatch[staff.m_delete]" form="stafflist_form"}</li>
        {/capture}
        {dropdown content=$smarty.capture.tools_list}
    {/if}
{/capture}
</form>
{/capture}

{capture name="adv_buttons"}
    
    {assign var="_title" value=__("staff_members")}
    
    <a class="btn cm-tooltip" href="{"staff.add"|fn_url}" title="{__("add_member")}"><i class="icon-plus"></i></a>
    
{/capture}



{include file="common/mainbox.tpl" title=$_title content=$smarty.capture.mainbox sidebar=$smarty.capture.sidebar adv_buttons=$smarty.capture.adv_buttons buttons=$smarty.capture.buttons content_id="manage_users"}