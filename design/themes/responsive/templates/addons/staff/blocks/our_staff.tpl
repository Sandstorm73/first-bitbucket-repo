{$obj_prefix = "`$block.block_id`000"}

<script type="text/javascript">
{literal}
(function(_, $) {

    $('.staff-email').on('click',function () {
	var id = $(this).attr("temp-data");
	
        $.ceAjax('request', fn_url('staff.get_email?staff_id=' + id), {
	    method: 'post',
	    callback: function(data) {
	        
		if (data.text != 'false') {
		    $('#staff_email_'+id).html('<a href="mailto:' + data.text +'">' + data.text +'</a>');
		} else {
		    $('#staff_email_'+id).html('{/literal}{__("no_email_found")}{literal}');
		}
	    } 
	});
    });
    
}(Tygh, Tygh.$));
{/literal}
</script>



{if $block.properties.outside_navigation == "Y"}
    <div class="owl-theme ty-owl-controls">
        <div class="owl-controls clickable owl-controls-outside" id="owl_outside_nav_{$block.block_id}">
            <div class="owl-buttons">
                <div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="ty-icon-left-open-thin"></i></div>
                <div id="owl_next_{$obj_prefix}" class="owl-next"><i class="ty-icon-right-open-thin"></i></div>
            </div>
        </div>
    </div>
{/if}

<div id="scroll_list_{$block.block_id}" class="owl-carousel">
    {foreach from=$staff item="member" name="for_members"}
            {include file="common/image.tpl" assign="object_img"  image_width=$block.properties.thumbnail_width image_height=$block.properties.thumbnail_width images=$member.main_pair no_ids=true lazy_load=true obj_id="scr_`$block.block_id`000`$member.staff_id`"}
            <div class="ty-center">
                <a >{$object_img nofilter}</a>
                <div><strong>{$member.firstname} {$member.lastname}</strong></div>
                <div>{$member.function}</div>
                <div id="staff_email_{$member.staff_id}"><a class="staff-email" temp-data="{$member.staff_id}">email</a></div>
            </div>
    {/foreach}
</div>
{include file="common/scroller_init.tpl" items=$staff prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`"}
